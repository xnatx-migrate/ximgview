The XNATImageViewer is the official HTML5 viewing module for [XNAT](http://www.xnat.org/).  It is built on [XTK](https://github.com/xtk/X#readme).


Features
----
* Visualize XNAT-hosted datasets in 2D and 3D directly from XNAT.
* Visualize Slicer scenes (.mrb).



Demo
--------------
Click [here](http://mokacreativellc.github.io/XNATImageViewer/Demo.html).



  
    